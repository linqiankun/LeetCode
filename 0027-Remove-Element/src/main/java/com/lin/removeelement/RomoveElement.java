package com.lin.removeelement;

/**
 * @author 九分石人
 */
public class RomoveElement {


    /**
     * 移除数组中指定元素
     *
     * @param nums 指定数组
     * @param val  指定元素
     * @return 剩余数组长度
     */
    public static int removeElement(int[] nums, int val) {
        int k = nums.length;
        for (int i = 0; i < k; i++) {
            if (nums[i] == val) {
                if (k - 1 - i >= 0) {
                    System.arraycopy(nums, i + 1, nums, i, k - 1 - i);
                }
                i--;
                k--;
            }
        }
        return k;
    }


    public static void main(String[] args) {
        int[] nums = {0, 1, 2, 2, 3, 0, 4, 2};
        int val = 2;
        int i = removeElement(nums, val);
        System.out.println(i);
    }
}
