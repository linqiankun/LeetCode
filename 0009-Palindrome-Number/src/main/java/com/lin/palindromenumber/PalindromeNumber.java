package com.lin.palindromenumber;

/**
 * 回文数
 *
 * @author 九分石人
 */
public class PalindromeNumber {


    /**
     * 回文数
     *
     * @param x 数
     * @return 是/否
     */
    public boolean isPalindrome(int x) {
        int num = x;
        if (x < 0) {
            return false;
        }
        if (x == 0) {
            return true;
        }
        int pes = 0;
        int p = 0;
        while (x != 0) {
            p = pes * 10 + x % 10;
            // 用计算后的数字与计算前的比较，判断是否溢出存储大小，溢出内存，提升效率
            if ((p - (x % 10)) / 10 != pes) {
                return false;
            }
            x = x / 10;
            pes = p;
        }
        if (num == pes) {
            return true;
        } else {
            return false;
        }
    }
}
