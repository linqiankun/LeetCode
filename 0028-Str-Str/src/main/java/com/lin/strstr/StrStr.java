package com.lin.strstr;

/**
 * @author 九分石人
 */
public class StrStr {

    /**
     * 截取字符字串
     *
     * @param haystack 原字符串
     * @param needle   截取的字符串
     * @return 返回截取字串的下标
     */
    public int strStr(String haystack, String needle) {
        if ("".equals(needle)) {
            return 0;
        }
        return haystack.indexOf(needle);
    }


}
