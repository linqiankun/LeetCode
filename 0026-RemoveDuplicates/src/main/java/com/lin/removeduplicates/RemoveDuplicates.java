package com.lin.removeduplicates;



/**
 * @author 九分石人
 */
public class RemoveDuplicates {

    /**
     * 移除数组中的重复项
     * @param nums 数组
     * @return 不重复项长度
     */
    public static int removeDuplicates(int[] nums) {
        int n = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length-n;) {
                if (nums[i] == nums[j]){
                    for (int k = j; k < nums.length-1; k++) {
                        nums[k]= nums[k+1];
                    }
                    n++;
                }
                if (nums[i] != nums[j]){
                    j++;
                }
            }
        }
        return nums.length-n;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,2};
        int i = removeDuplicates(nums);
        System.out.println(i);
    }
}
