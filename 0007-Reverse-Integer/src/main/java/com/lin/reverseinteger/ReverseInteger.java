package com.lin.reverseinteger;

/**
 * 整数反转
 *
 * @author 九分石人
 */
public class ReverseInteger {


    /**
     * 整数反转
     *
     * @param x 目标元素
     * @return 反转后的元素
     */
    public int reverse(int x) {
        int pes = 0;
        int p = 0;
        while (x != 0) {
            p = pes * 10 + x % 10;
            //用计算后的数字与计算前的比较，判断是否溢出存储大小
            if ((p - (x % 10)) / 10 != pes) {
                return 0;
            }
            x = x / 10;
            pes = p;
        }
        return pes;
    }


}
