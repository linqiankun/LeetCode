package com.lin.addtwonumbers;


/**
 * @author linqiankun
 * @date 2020-06-23
 */
public class AddTwoNumbers {


    /**
     * 两数相加
     *
     * @param l1 链表1
     * @param l2 链表2
     * @return 返回结果链表的头节点
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode listNode = new ListNode(l1.val);
        ListNode c = listNode;
        int n = 0;
        do {
            int a = 0;
            if (l1 != null) {
                a = l1.val;
            }
            int b = 0;
            if (l2 != null) {
                b = l2.val;
            }
            int sum = a + b + n;
            c.val = sum % 10;
            n = sum / 10;
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
            if (l1 != null || l2 != null || n != 0) {
                c.next = new ListNode(n);
                c = c.next;
            }

        } while (l1 != null || l2 != null);
        return listNode;
    }
}


