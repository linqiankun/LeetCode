package com.lin.addtwonumbers;

public class ListNode {
    int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }
}