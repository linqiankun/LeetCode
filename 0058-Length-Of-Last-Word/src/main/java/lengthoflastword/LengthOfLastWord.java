package lengthoflastword;

/**
 * @author 九分石人
 */
public class LengthOfLastWord {

    /**
     * 按空格切割后最后字符串的长度
     *
     * @param s 原始字符串
     * @return 长度
     */
    public static int lengthOfLastWord(String s) {
        String[] s1 = s.split(" ");
        if (s1.length == 0) {
            return 0;
        } else {
            return s1[s1.length - 1].length();
        }
    }

    public static void main(String[] args) {
        int i = lengthOfLastWord("");
        System.out.println(i);
    }
}
