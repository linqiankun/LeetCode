package com.lin.houserobberiii;

/**
 * @author 九分石人
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
